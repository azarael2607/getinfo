package com.example.ricardo.cursoandroidi;

import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class DetalhesUsuario extends Fragment {

    private Button btnExibirCidades;

    private View.OnClickListener btnExibirCidadesClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (MainActivity.liberado) {

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new ListaCidades())
                        .addToBackStack(null).commit();
            }
        }
    };

    public DetalhesUsuario(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detalhes_usuario, container, false);

        btnExibirCidades = (Button) root.findViewById(R.id.btnCidades);
        btnExibirCidades.setOnClickListener(btnExibirCidadesClick);

        return root;
    }
}
