package com.example.ricardo.cursoandroidi;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class CidadeWiki extends Fragment {

    private WebView wvNav;
    public static String url = "";

    public CidadeWiki() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cidade_wiki, container, false);

        wvNav = (WebView) root.findViewById(R.id.wvNav);
        wvNav.setWebViewClient(new Callback());
        wvNav.loadUrl(url);

        return root;
    }

    private class Callback extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }
    }
}
