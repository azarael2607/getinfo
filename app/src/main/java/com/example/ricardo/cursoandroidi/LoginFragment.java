package com.example.ricardo.cursoandroidi;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    private EditText edtLogin;
    private EditText edtSenha;
    private Button btnLogar;

    View.OnClickListener btnLogarClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (edtLogin.getText().toString().toUpperCase().equals("RICARDO")){
                if (edtSenha.getText().toString().equals("123")){
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, new DetalhesUsuario())
                            .addToBackStack(null).commit();
                } else {
                    Toast.makeText(getActivity(), "Senha Errada", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Login Errado", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);

        edtLogin = (EditText) root.findViewById(R.id.edtLogin);
        edtSenha = (EditText) root.findViewById(R.id.edtSenha);
        btnLogar = (Button) root.findViewById(R.id.btnLogar);

        btnLogar.setOnClickListener(btnLogarClick);

        return root;
    }
}
