package com.example.ricardo.cursoandroidi;

import com.example.ricardo.cursoandroidi.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        ivLogo.startAnimation(animation);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                JSonParser parser = new JSonParser();
                Log.d("Baixando", "Começou");
                JSONObject jsonObject = parser.getJSONFromUrl("http://api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=demo");
                Log.d("Baixou", "Terminou");
                try {
                    Log.d("Baixou", jsonObject.toString());
                    MainActivity.array = jsonObject.getJSONArray("geonames");
                    MainActivity.liberado = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (MainActivity.liberado) {
                    finish();
                    Intent intent = new Intent();
                    intent.setClass(Splash.this, MainActivity.class);
                    startActivity(intent);
                    cancel();
                }
            }
        }, 2000, 200);
    }

}
