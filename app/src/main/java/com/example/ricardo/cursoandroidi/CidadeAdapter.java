package com.example.ricardo.cursoandroidi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ricardo on 15/07/2015.
 */
public class CidadeAdapter extends ArrayAdapter<Cidade> {

    private final Context context;
    private final ArrayList<Cidade> list;

    public CidadeAdapter(Context context, ArrayList<Cidade> list){
        super(context, R.layout.lista_cidade, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View item = convertView;
        CidadeAdapterHolder pah;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(R.layout.lista_cidade, null);
            pah = new CidadeAdapterHolder(item);
            item.setTag(pah);
        } else {
            pah = (CidadeAdapterHolder) convertView.getTag();
        }

        pah.tvItemNome.setText(this.list.get(position).nome);
        pah.tvItemPopulacao.setText(this.list.get(position).populacao);

        return item;
    }

    class CidadeAdapterHolder {
        public TextView tvItemNome;
        public TextView tvItemPopulacao;
        public CidadeAdapterHolder(View base) {
            tvItemNome = (TextView) base.findViewById(R.id.tvNomeCidade);
            tvItemPopulacao = (TextView) base.findViewById(R.id.tvPopulacao);
        }
    }
}
