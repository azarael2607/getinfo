package com.example.ricardo.cursoandroidi;

import android.app.Activity;
import android.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListaCidades extends Fragment {

    private ArrayList<Cidade> list;
    private ListView lvLista;

    private AdapterView.OnItemClickListener  itemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CidadeWiki.url = "http://" + list.get(position).url;
            Log.d("url", CidadeWiki.url);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new CidadeWiki())
                    .addToBackStack(null).commit();
        }
    };

    public ListaCidades() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_lista_cidades, container, false);
        lvLista = (ListView) root.findViewById(R.id.lvLista);
        lvLista.setOnItemClickListener(itemClick);

        list = new ArrayList<>();

        JSONArray array = ((MainActivity) getActivity()).array;

        for (int i = 0; i < array.length(); i++){
            Cidade cidade = new Cidade();
            try {
                JSONObject jsonObject = array.optJSONObject(i);
                cidade.nome = jsonObject.getString("name");
                cidade.populacao = jsonObject.getString("population");
                cidade.url = jsonObject.getString("wikipedia");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            list.add(cidade);
        }

        lvLista.setAdapter(new CidadeAdapter(getActivity(), list));

        return root;
    }
}
